// main.js
document.addEventListener('DOMContentLoaded', function () {
    const countryInput = document.getElementById('countryInput');
    const countryInfoContainer = document.getElementById('countryInfo');
    const searchBtn = document.getElementById('searchBtn');
    const clearBtn = document.getElementById('clearBtn');
  
    searchBtn.addEventListener('click', function () {
      const enteredCountryName = countryInput.value.trim();
  
      if (enteredCountryName) {
        fetch(`https://restcountries.com/v3.1/name/${enteredCountryName}?limit=1`)
          .then(response => response.json())
          .then(data => mostrarInformacionPais(data))
          .catch(error => console.error('Error al obtener la información del país', error));
      } else {
        alert('Ingrese el nombre de un país.');
      }
    });
  
    clearBtn.addEventListener('click', function () {
      countryInfoContainer.innerHTML = '';
      countryInput.value = '';
    });
  
    function mostrarInformacionPais(data) {
      if (data && data.length > 0) {
        const country = data[0];
        const languages = country.languages ? Object.values(country.languages).join(', ') : 'Desconocido';
  
        const content = `
          <h2>${country.name.common}</h2>
          <p><strong>Capital:</strong> ${country.capital ? country.capital[0] : 'Desconocido'}</p>
          <p><strong>Lenguajes:</strong> ${languages}</p>
        `;
  
        countryInfoContainer.innerHTML = content;
      } else {
        alert('No se encontró información para el país ingresado.');
      }
    }
  });
  